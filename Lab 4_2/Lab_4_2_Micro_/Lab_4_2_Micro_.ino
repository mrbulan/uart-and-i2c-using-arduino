#include <Adafruit_Sensor.h>

#include <Adafruit_LSM303_U.h>

#include <Wire.h>
#include <SoftwareSerial.h>
#include <Keypad.h> 

Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);
Adafruit_LSM303_Mag_Unified mag = Adafruit_LSM303_Mag_Unified(12345);
SoftwareSerial mySerial(10, 11); // RX, TX

void setup() {
  Serial.begin(9600);
  while(!Serial)
  {
    ;
  }
  mag.enableAutoRange(true);
  Serial.println("Accelerometer Test"); Serial.println("");
  
  /* Initialise the sensor */
  if(!accel.begin() || !mag.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(1);
  }

  mySerial.begin(4800);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(mySerial.available())
  {
    char charGotten = mySerial.read();
    if(charGotten == '1')
    {
      getAcel();
      mySerial.write("Acking 1");
    }
    if(charGotten == '2')
    {
      getMag();
      mySerial.write("Acking 2");
    }
    if(charGotten == '3')
    {
      getAcel();
      getMag();
      mySerial.write("Acking 3");
    }
  }
  delay(500);

}

void getAcel()
{
  sensors_event_t event; 
  accel.getEvent(&event);
 
  /* Display the results (acceleration is measured in m/s^2) */
  Serial.print("X: "); Serial.print(event.acceleration.x); Serial.print("  ");
  Serial.print("Y: "); Serial.print(event.acceleration.y); Serial.print("  ");
  Serial.print("Z: "); Serial.print(event.acceleration.z); Serial.print("  ");Serial.println("m/s^2 ");
}

void getMag()
{
    sensors_event_t event; 
  mag.getEvent(&event);
 
  /* Display the results (magnetic vector values are in micro-Tesla (uT)) */
  Serial.print("X: "); Serial.print(event.magnetic.x); Serial.print("  ");
  Serial.print("Y: "); Serial.print(event.magnetic.y); Serial.print("  ");
  Serial.print("Z: "); Serial.print(event.magnetic.z); Serial.print("  ");Serial.println("uT");
}

